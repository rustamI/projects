package com.sbta.api.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sbta.models.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class Converter {
    private static final Logger logger = LoggerFactory.getLogger(Converter.class);

    public static Transaction getTransactionObjectByRequestBody(String data) throws Exception {
        logger.trace("getTransactionObjectByRequestBody() begin");
        logger.debug("input data: data - {}",data);
        Transaction result = null;
        ObjectMapper objectMapper = new ObjectMapper();
        result = objectMapper.readValue(data, Transaction.class);
        logger.debug("returns: result - {}",result);
        logger.trace("getTransactionObjectByRequestBody() end");
        return result;
    }

    public static String round(BigDecimal value) {
        logger.trace("round() begin");
        String result = value.setScale(2, BigDecimal.ROUND_HALF_UP).toString();
        logger.debug("returns: result - {}",result);
        logger.trace("round() end");
        return result;
    }
}
