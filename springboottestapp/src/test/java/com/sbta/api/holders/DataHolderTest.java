package com.sbta.api.holders;

import com.sbta.models.Statistic;
import com.sbta.models.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;

import com.test.utils.Colors;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class DataHolderTest {

    @Test
    public void getInstanceHasSameInstanceOfObject() {
        String requestID = Colors.BLUE_BOLD + "getInstanceHasSameInstanceOfObject" + Colors.RESET;
        System.out.println(requestID + " start");
        DataHolder.getInstance().clear();
        AccessToDataHolderRunnable accessToDataHolderRunnable1 = new AccessToDataHolderRunnable();
        AccessToDataHolderRunnable accessToDataHolderRunnable2 = new AccessToDataHolderRunnable();
        AccessToDataHolderRunnable accessToDataHolderRunnable3 = new AccessToDataHolderRunnable();
        AccessToDataHolderRunnable accessToDataHolderRunnable4 = new AccessToDataHolderRunnable();
        Thread t1 = new Thread(accessToDataHolderRunnable1);
        Thread t2 = new Thread(accessToDataHolderRunnable2);
        Thread t3 = new Thread(accessToDataHolderRunnable3);
        Thread t4 = new Thread(accessToDataHolderRunnable4);

        t1.start();
        t2.start();
        t3.start();
        t4.start();

        assertEquals(accessToDataHolderRunnable1.getDataHolder(), accessToDataHolderRunnable2.getDataHolder());
        assertEquals(accessToDataHolderRunnable2.getDataHolder(), accessToDataHolderRunnable3.getDataHolder());
        assertEquals(accessToDataHolderRunnable3.getDataHolder(), accessToDataHolderRunnable4.getDataHolder());
        System.out.println(requestID + " end");
    }

    @Test
    public void pushInMultipalThreadIsSuccess() {
        String requestID = Colors.BLUE_BOLD + "pushInMultipalThreadIsSuccess" + Colors.RESET;
        System.out.println(requestID + " start");
        DataHolder.getInstance().clear();
        AccessToDataHolderRunnable accessToDataHolderRunnable1
                = new AccessToDataHolderRunnable(new Transaction("100.0", Instant.now().minusSeconds(2L).toString()));
        AccessToDataHolderRunnable accessToDataHolderRunnable2
                = new AccessToDataHolderRunnable(new Transaction("10.0", Instant.now().minusSeconds(7L).toString()));
        AccessToDataHolderRunnable accessToDataHolderRunnable3
                = new AccessToDataHolderRunnable(new Transaction("100.4", Instant.now().minusSeconds(9L).toString()));
        AccessToDataHolderRunnable accessToDataHolderRunnable4
                = new AccessToDataHolderRunnable(new Transaction("100.5", Instant.now().minusSeconds(25L).toString()));

        Thread t1 = new Thread(accessToDataHolderRunnable1);
        Thread t2 = new Thread(accessToDataHolderRunnable2);
        Thread t3 = new Thread(accessToDataHolderRunnable3);
        Thread t4 = new Thread(accessToDataHolderRunnable4);

        t1.start();
        t2.start();
        t3.start();
        t4.start();

        assertFalse(accessToDataHolderRunnable1.isFailed());
        assertFalse(accessToDataHolderRunnable2.isFailed());
        assertFalse(accessToDataHolderRunnable3.isFailed());
        assertFalse(accessToDataHolderRunnable4.isFailed());
        System.out.println(requestID + " end");
    }

    @Test
    public void clearIsNotFailedInAnyThread() {
        String requestID = Colors.BLUE_BOLD + "clear()" + Colors.RESET;
        System.out.println(requestID + " start");
        DataHolder.getInstance().clear();
        AccessToDataHolderRunnable accessToDataHolderRunnable1
                = new AccessToDataHolderRunnable(new Transaction("100.0", Instant.now().minusSeconds(2L).toString()));
        AccessToDataHolderRunnable accessToDataHolderRunnable2
                = new AccessToDataHolderRunnable(new Transaction("10.0", Instant.now().minusSeconds(7L).toString()));
        AccessToDataHolderRunnable accessToDataHolderRunnable3
                = new AccessToDataHolderRunnable(new Transaction("100.4", Instant.now().minusSeconds(9L).toString()));
        AccessToDataHolderRunnable accessToDataHolderRunnable4
                = new AccessToDataHolderRunnable(new Transaction("100.5", Instant.now().minusSeconds(25L).toString()));

        accessToDataHolderRunnable1.run();
        accessToDataHolderRunnable2.run();
        accessToDataHolderRunnable3.run();
        accessToDataHolderRunnable4.run();

        ClearAllRunnable r1 = new ClearAllRunnable();
        ClearAllRunnable r2 = new ClearAllRunnable();
        ClearAllRunnable r3 = new ClearAllRunnable();

        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);
        Thread t3 = new Thread(r3);

        t1.start();
        t2.start();
        t3.start();

        assertFalse(r1.isFailed());
        assertFalse(r2.isFailed());
        assertFalse(r3.isFailed());
        DataHolder.getInstance().clear();
        System.out.println(requestID + " end");
    }

    @Test
    public void getStatisticBeforeClear() {

        String requestID = Colors.BLUE_BOLD + "getStatisticBeforeClear" + Colors.RESET;
        System.out.println(requestID + " start");
        DataHolder.getInstance().clear();
        AccessToDataHolderRunnable accessToDataHolderRunnable1
                = new AccessToDataHolderRunnable(new Transaction("100.0", Instant.now().minusSeconds(2L).toString()));
        AccessToDataHolderRunnable accessToDataHolderRunnable2
                = new AccessToDataHolderRunnable(new Transaction("10.0", Instant.now().minusSeconds(7L).toString()));
        AccessToDataHolderRunnable accessToDataHolderRunnable3
                = new AccessToDataHolderRunnable(new Transaction("100.4", Instant.now().minusSeconds(9L).toString()));
        AccessToDataHolderRunnable accessToDataHolderRunnable4
                = new AccessToDataHolderRunnable(new Transaction("100.5", Instant.now().minusSeconds(25L).toString()));

        Statistic expectedStatistic = new Statistic("310.90", "77.73", "100.50", "10.00", 4);

        accessToDataHolderRunnable1.run();
        accessToDataHolderRunnable2.run();
        accessToDataHolderRunnable3.run();
        accessToDataHolderRunnable4.run();

        assertEquals(expectedStatistic, DataHolder.getInstance().getStatistic());
        DataHolder.getInstance().clear();
        System.out.println(requestID + " end");
    }

    @Test
    public void getStatisticRightAfterClear() {
        String requestID = Colors.BLUE_BOLD + "getStatisticRightAfterClear" + Colors.RESET;
        System.out.println(requestID + " start");
        DataHolder.getInstance().clear();
        AccessToDataHolderRunnable accessToDataHolderRunnable1
                = new AccessToDataHolderRunnable(new Transaction("100.0", Instant.now().minusSeconds(2L).toString()));
        AccessToDataHolderRunnable accessToDataHolderRunnable2
                = new AccessToDataHolderRunnable(new Transaction("10.0", Instant.now().minusSeconds(7L).toString()));
        AccessToDataHolderRunnable accessToDataHolderRunnable3
                = new AccessToDataHolderRunnable(new Transaction("100.4", Instant.now().minusSeconds(9L).toString()));
        AccessToDataHolderRunnable accessToDataHolderRunnable4
                = new AccessToDataHolderRunnable(new Transaction("100.5", Instant.now().minusSeconds(25L).toString()));

        Statistic expectedStatistic = new Statistic("0.00", "0.00", "0.00", "0.00", 0);

        accessToDataHolderRunnable1.run();
        accessToDataHolderRunnable2.run();
        accessToDataHolderRunnable3.run();
        accessToDataHolderRunnable4.run();

        DataHolder.getInstance().clear();

        assertEquals(expectedStatistic, DataHolder.getInstance().getStatistic());
        DataHolder.getInstance().clear();
        System.out.println(requestID + " end");
    }


    private class AccessToDataHolderRunnable implements Runnable {
        private DataHolder dataHolder = null;
        private Collection<Transaction> transactions = new ArrayList<>();
        private boolean failed = false;

        AccessToDataHolderRunnable() {
        }

        AccessToDataHolderRunnable(Transaction transaction) {
            transactions.add(transaction);
        }

        @Override
        public void run() {
            dataHolder = DataHolder.getInstance();
            try {
                transactions.forEach((item) ->
                        dataHolder.push(item));
            } catch (Exception e) {
                failed = true;
            }
        }

        DataHolder getDataHolder() {
            return dataHolder;
        }

        boolean isFailed() {
            return failed;
        }
    }

    private class ClearAllRunnable implements Runnable {
        private boolean failed = false;

        @Override
        public void run() {
            DataHolder dataHolder = DataHolder.getInstance();
            try {
                dataHolder.clear();
            } catch (Exception e) {
                failed = true;
            }
        }

        boolean isFailed() {
            return failed;
        }

    }
}