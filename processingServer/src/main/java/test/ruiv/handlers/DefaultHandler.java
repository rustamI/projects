package test.ruiv.handlers;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.commons.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.ruiv.controllers.v1.AbstractController;
import test.ruiv.model.Model;
import test.ruiv.processors.Methods;
import test.ruiv.processors.Processor;

import java.io.IOException;
import java.util.*;

/**
 * Default Handler executes corresponding controller by it's name and provided method
 * <br/>Example:
 * <pre> </>http://"server"/"conroller"/"method"?param1 = value1&param2=value </pre>
 */
public class DefaultHandler implements HttpHandler {
    protected static final Logger logger = LoggerFactory.getLogger(DefaultHandler.class);

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        logger.trace("handle() begin");
        Map<String, Object> data = new HashMap<>();

        try {
            Processor processor = (Processor) Methods.valueOf(httpExchange.getRequestMethod()).getProcessor().newInstance();
            String[] paths = httpExchange.getRequestURI().getPath().split("/");
            String controllerName = paths[2];
            data.put("controllerName", controllerName);
            data.put("modelClass", Class.forName("test.ruiv.model." + WordUtils.capitalize(controllerName)));
            String methodName = paths[3];
            data.put("methodName", methodName);
            logger.debug("splited path - {}", Arrays.asList(paths));
            AbstractController<Model> controller = getController(paths);
            data.put("controller", controller);
            processor.getValue(httpExchange, data);
        } catch (Exception e) {
            httpExchange.sendResponseHeaders(500, -1);
            logger.error("error ocurred", e);
        }
        logger.trace("handle() end");
    }

    /**
     *  It creates controller by given path
     * @param paths
     * @return Instance of Controller
     * @throws Exception
     */
    private AbstractController<Model> getController(String[] paths) throws Exception {
        logger.trace("getConroller() begin");
        String version = paths[1];
        String controllerName = paths[2];
        AbstractController<Model> controller = null;
        Class clazzController = Class.forName("test.ruiv.controllers." + version + "." + WordUtils.capitalize(controllerName) + "Controller");
        controller = (AbstractController<Model>) clazzController.newInstance();
        logger.debug("returns: controller - {}", controller);
        logger.trace("getcontroller() end");
        return controller;
    }
}
