package test.ruiv.dispatchers;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.ruiv.handlers.DefaultHandler;
import test.ruiv.handlers.V2Handler;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * It's intended to execute specified handler for particular path
 */
public class DefaultDispatcher {
    protected static final Logger logger = LoggerFactory.getLogger(DefaultDispatcher.class);
    private HttpServer httpServer;
    private Map<String, HttpHandler> controllersBinding = new HashMap<>();

    /**
     * Constructor binds default handler to /v1/ and /v2/ paths
     */
    public DefaultDispatcher(HttpServer httpServer) {
        this.httpServer = httpServer;
        logger.trace("DefaultDispatcher has been initialized\n" +
                "http server - {}:{}", httpServer.getAddress().getHostName(), httpServer.getAddress().getPort());
        controllersBinding.put("/v1/", new DefaultHandler());
        httpServer.createContext("/v1/", new DefaultHandler());
        httpServer.createContext("/v2/", new V2Handler());
    }

    /**
     * Allows to specify mapping between path and its handler in API
     * @param httpServer
     * @param bindings
     */
    public DefaultDispatcher(HttpServer httpServer, Map<String, HttpHandler> bindings) {
        this.httpServer = httpServer;
        this.controllersBinding = bindings;
        logger.debug("path handlers initialization started - ");
        for(Map.Entry<String, HttpHandler> entry : bindings.entrySet()) {
            String key = entry.getKey();
            HttpHandler value = entry.getValue();
            logger.debug("path:\"{}\" handler:\"{}\"",key,value.getClass());
            httpServer.createContext(key, value);
        }
        logger.debug("- path handlers initialization end.");
    }

    private static Map<String, HttpHandler> loadConfiguration() {
        Map<String, HttpHandler> result = new HashMap<>();
        InputStream input = null;
        try{
            input = new FileInputStream("paths.json");
            //JsonParser jsonParser = new JsonParser();
            Gson gson = new Gson();
            result.putAll(gson.fromJson(new InputStreamReader(input), Map.class));
            System.out.println("result - " + result);
        }catch (IOException e) {
            logger.error("dispatcher config load is failed ", e);
        }

        return result;
    }

}
