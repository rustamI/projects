package test.ruiv.processors;

import com.sun.net.httpserver.HttpExchange;
import org.apache.http.client.utils.URLEncodedUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.ruiv.controllers.v1.AbstractController;
import test.ruiv.model.Model;

import java.lang.reflect.Method;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractMethodProcessor implements Processor {
    protected Logger logger = LoggerFactory.getLogger(AbstractMethodProcessor.class);

    private Object params;

    public abstract String getValue(HttpExchange httpExchange, Map<String, Object> data) throws Exception;

    /**
     * It retrieves params from given URI
     *
     * @param uri requested URI from client
     * @return list of params in the URI
     */
    public Map<String, String> getUrlParams(URI uri) {
        logger.trace("getUrlParams() begin");
        logger.debug("input params: \nurl - {}", uri);
        Map<String, String> result = new HashMap<>();
        URLEncodedUtils.parse(uri, String.valueOf(Charset.forName("UTF-8")))
                .forEach(item -> result.put(item.getName(), item.getValue()));
        logger.debug("getUrlParams() end");
        return result;
    }

    protected abstract <T> T getMethodParams();

    protected Object executeMethod(AbstractController<Model> controller, String methodName, Class... parameterClass) throws Exception {
        logger.trace("executeMethod() begin");
        Object result = null;
        Method method = null;
        Class controllerClazz = controller.getClass();
        if (this.params instanceof List) {
            List methodParameter = (List) this.params;
            if (methodParameter.isEmpty()) {
                method = controllerClazz.getMethod(methodName);
            } else {
                method = controllerClazz.getMethod(methodName, parameterClass);
            }
            if (methodParameter.size() == 0) {
                result = method.invoke(controller);
            } else {
                result = method.invoke(controller, methodParameter);
            }
        } else {
            Object methodParameter = this.params;
            if (methodParameter == null) {
                method = controllerClazz.getMethod(methodName);
            } else {
                method = controllerClazz.getMethod(methodName, parameterClass);
            }
            if (methodParameter == null) {
                result = method.invoke(controller);
            } else {
                result = method.invoke(controller, methodParameter);
            }
        }
        logger.trace("executeMethod() end");
        return result;
    }

    protected void setParams() {
        this.params = getMethodParams();
    }
}
