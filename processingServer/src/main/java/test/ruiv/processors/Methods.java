package test.ruiv.processors;

import test.ruiv.processors.GetProcessor;
import test.ruiv.processors.PostProcessor;

public enum Methods {
    GET(GetProcessor.class),
    POST(PostProcessor.class),
    DELETE(DeleteProcessor.class)/*,
    PUT,
    PATCH*/;

    private Class clazz;

    private Methods(Class clazz) {
        this.clazz = clazz;
    }

    public Class getProcessor() {
        return this.clazz;
    }
}
