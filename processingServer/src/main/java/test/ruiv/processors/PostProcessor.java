package test.ruiv.processors;

import com.google.gson.*;
import com.sun.net.httpserver.HttpExchange;
import test.ruiv.controllers.v1.AbstractController;
import test.ruiv.model.Model;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class PostProcessor extends AbstractMethodProcessor implements Processor {
    private Object data;

    @Override
    public String getValue(HttpExchange httpExchange, Map<String, Object> data) throws Exception {
        logger.trace("PostProcessor begin");
        InputStream request = httpExchange.getRequestBody();
        OutputStream response = httpExchange.getResponseBody();
        InputStreamReader inSR = new InputStreamReader(request);
        Gson gson = new Gson();
        JsonElement reqJsonElement = new JsonParser().parse(inSR);
        Class modelClass = (Class) data.get("modelClass");
        JsonObject jsonObject = reqJsonElement.getAsJsonObject();
        List<Model> models = new ArrayList<>();
        for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
            String key = entry.getKey();
            JsonElement value = entry.getValue();
            Class requestClass = Class.forName(key);
            System.out.println(key);
            if (value.isJsonArray()) {
                JsonArray jsonArray = value.getAsJsonArray();
                jsonArray.forEach(item -> models.add((Model) gson.fromJson(item, requestClass)));
            } else {
                models.add((Model) gson.fromJson(value, requestClass));
            }
        }
        this.data = models;
        AbstractController<Model> controller = (AbstractController<Model>) data.get("controller");
        String methodName = (String) data.get("methodName");
        setParams();
        Object value = executeMethod(controller, methodName, models.get(0).getClass());

        //System.out.println("models - "+ models);
        httpExchange.getResponseHeaders().add("Content-type", "application/json");
        httpExchange.sendResponseHeaders(200, value.toString().length());
        response.write(value.toString().getBytes());
        response.close();
        logger.trace("PostProcessor end");
        return null;
    }

    @Override
    protected <T> T getMethodParams() {
        if (this.data instanceof List) {
            List methodParameter = (List) this.data;
            if (methodParameter.size() == 1) {
                return (T) ((List) this.data).get(0);
            }
        }
        return (T) this.data;
    }
}
