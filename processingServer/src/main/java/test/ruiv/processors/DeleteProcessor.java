package test.ruiv.processors;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.HttpExchange;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Map;

public class DeleteProcessor extends AbstractMethodProcessor implements Processor {
    @Override
    public String getValue(HttpExchange httpExchange, Map<String, Object> data) throws Exception {
        InputStream request = httpExchange.getRequestBody();
        OutputStream response = httpExchange.getResponseBody();
        InputStreamReader inSR = new InputStreamReader(request);
        JsonElement reqJsonElement =  new JsonParser().parse(inSR);
        httpExchange.getResponseHeaders().add("Content-type", "application/json");
        httpExchange.sendResponseHeaders(200, reqJsonElement.toString().length());
        response.write(reqJsonElement.toString().getBytes());
        //response.write(reqJsonElement.toString().getBytes());
        response.close();
        return null;
    }

    @Override
    protected <T> T getMethodParams() {
        return null;
    }
}
