package com.sbta.api.validators;

import java.util.Map;

public interface Validator {
    //TODO it can be revisited to be more convenient in manager
    enum Status {
        PASSED,
        FAILED ,
        ERROR_OCCURRED;
    }

    boolean validate(Map context) throws Exception;
    Validator setNext(Validator validator);
}
