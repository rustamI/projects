package com.sbta.models;

import java.util.Objects;

public class Statistic {
    private String sum;
    private String avg;
    private String max;
    private String min;
    private int count = 0;

    public Statistic(String sum, String avg, String max, String min, int count) {
        this.sum = sum;
        this.avg = avg;
        this.max = max;
        this.min = min;
        this.count = count;
    }


    public String getSum() {
        return sum;
    }

    public String getAvg() {
        return avg;
    }

    public String getMax() {
        return max;
    }

    public String getMin() {
        return min;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "Statistic{" +
                "sum=" + sum +
                ", avg=" + avg +
                ", max=" + max +
                ", min=" + min +
                ", count=" + count +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Statistic)) return false;
        Statistic statistic = (Statistic) o;
        return getCount() == statistic.getCount() &&
                Objects.equals(getSum(), statistic.getSum()) &&
                Objects.equals(getAvg(), statistic.getAvg()) &&
                Objects.equals(getMax(), statistic.getMax()) &&
                Objects.equals(getMin(), statistic.getMin());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSum(), getAvg(), getMax(), getMin(), getCount());
    }
}
