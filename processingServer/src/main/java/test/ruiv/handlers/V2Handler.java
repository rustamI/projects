package test.ruiv.handlers;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.ruiv.processors.Methods;
import test.ruiv.processors.Processor;

import java.io.IOException;
import java.io.OutputStream;

import static java.lang.System.out;

public class V2Handler implements HttpHandler {
    protected static final Logger logger = LoggerFactory.getLogger(V2Handler.class);

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        logger.trace("handle() begin");
        OutputStream outputStream = httpExchange.getResponseBody();
        httpExchange.sendResponseHeaders(501, -1);
        logger.trace("handle() end");
    }
}
