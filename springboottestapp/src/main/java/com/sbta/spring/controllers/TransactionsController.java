package com.sbta.spring.controllers;

import com.sbta.api.Collector;
import com.sbta.api.holders.DataHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TransactionsController {

    @Autowired
    private Collector collector;

    @RequestMapping(value = "/transactions", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<String> create(@RequestBody String data) {
        ResponseEntity<String> responseEntity = collector.add(data);
        return responseEntity;
    }

    @RequestMapping(value = "/transactions", method = RequestMethod.DELETE, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public String delete() {
        // no needs to have more than enough extensibility
        collector.clear();
        return "";
    }
}
