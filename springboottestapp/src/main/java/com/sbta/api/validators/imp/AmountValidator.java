package com.sbta.api.validators.imp;

import com.sbta.models.Transaction;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.util.Map;

public class AmountValidator extends AbstractValidator {

    @Override
    protected boolean doValidation(Map context) {
        logger.trace("doValidation() begin");
        Transaction incomeTransaction = (Transaction) context.get("transaction");
        try {
            BigDecimal amount = new BigDecimal(incomeTransaction.getAmount());
        }catch ( NumberFormatException e) {
            //422 – if any of the fields are not parsable or the transaction date is in the future
            context.put("httpStatus", HttpStatus.UNPROCESSABLE_ENTITY);
            context.put("errorInfo", e);
            next = null;
            logger.trace("doValidation() end");
            return false;
        }
        logger.trace("doValidation() end");
        return true;
    }
}
