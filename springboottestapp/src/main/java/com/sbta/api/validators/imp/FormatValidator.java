package com.sbta.api.validators.imp;

import com.sbta.api.utils.Converter;
import com.sbta.models.Transaction;
import org.springframework.http.HttpStatus;

import java.io.PrintWriter;
import java.util.Map;

public class FormatValidator extends AbstractValidator {
    @Override
    protected synchronized boolean doValidation(Map context) {
        logger.trace("doValidation() begin");
        //TODO consider elegant way of debugging, it might have too much data inside
        logger.debug("input params: context - {}", context);
        String data = (String) context.get("data");
        try{
            Transaction transaction = Converter.getTransactionObjectByRequestBody(data);
            // actually, it's quite strange to create something in validator, it's done just to save some time
            context.put("transaction", transaction);
        }catch (Exception e){
            context.put("httpStatus", HttpStatus.BAD_REQUEST);
            context.put("errorInfo", e);
            // next validators obviously will fail
            next = null;
            return false;
        }
        return true;
    }
}
