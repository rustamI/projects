package test.ruiv.processors;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.HttpExchange;
import test.ruiv.controllers.v1.AbstractController;
import test.ruiv.model.Model;

import java.io.OutputStream;
import java.util.*;

public class GetProcessor extends AbstractMethodProcessor implements Processor {
    private  Map<String, String> urlParams = new HashMap<>();
    @Override
    public String getValue(HttpExchange httpExchange, Map<String, Object> data) throws Exception {
        logger.trace("getValue() begin");
        this.urlParams.putAll(getUrlParams(httpExchange.getRequestURI()));

        OutputStream response = httpExchange.getResponseBody();
        Gson gson = new Gson();

        JsonParser jsonParser = new JsonParser();
        Class controllerClass = (Class) data.get("controllerClass");
        AbstractController<Model> controller = (AbstractController<Model>) data.get("controller");
        String methodName = (String) data.get("methodName");
        setParams();
        Object value = executeMethod(controller, methodName, Object.class);
        String jsonString = gson.toJson(value);
        JsonElement jsonElement = jsonParser.parse(gson.toJson(value));
        String objectType = "";
        if (value instanceof List) {
            List<Model> objects = (List<Model>) value;
            if (!objects.isEmpty()) {
                objectType = objects.get(0).getClass().getName();
            } else {
               objectType = controllerClass.getName();
            }
        } else {
            objectType = value.getClass().getName();
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.add(objectType, jsonElement);
        httpExchange.getResponseHeaders().add("Content-type", "application/json");
        httpExchange.sendResponseHeaders(200, jsonObject.toString().length());
        response.write(jsonObject.toString().getBytes());
        response.close();
        return null;
    }

    @Override
    protected Collection getMethodParams() {
        final Collection params = this.urlParams.values();
        return new ArrayList(){{addAll(params);}};
    }

}
