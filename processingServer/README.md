Test processing RESTfull standalone application

Requirements
1. Apache Maven 3.5.4 or upper
2. Java version "1.8.0_171" or upper
3. Dependencies
 +- junit:junit:jar:4.11:test
 |  \- org.hamcrest:hamcrest-core:jar:1.3:test
 +- org.mockito:mockito-all:jar:1.9.5:test
 +- com.google.code.gson:gson:jar:2.8.5:compile
 +- com.h2database:h2:jar:1.4.197:compile
 +- org.slf4j:slf4j-api:jar:1.7.25:compile
 +- org.slf4j:slf4j-log4j12:jar:1.7.25:compile
 |  \- log4j:log4j:jar:1.2.17:compile
 +- org.apache.httpcomponents:httpclient:jar:4.5.3:compile
 |  +- org.apache.httpcomponents:httpcore:jar:4.4.6:compile
 |  +- commons-logging:commons-logging:jar:1.2:compile
 |  \- commons-codec:commons-codec:jar:1.9:compile
 \- org.apache.commons:commons-text:jar:1.5:compile
    \- org.apache.commons:commons-lang3:jar:3.8.1:compile
	
HOW TO START
execute 
 1. build.bat
 2. run.bat
 
 by default server will be started with http://localhost:8081
 
 HOW TO TEST
  1. import soapUI project REST-soapui-project.xml
  2. Select one of requests and click to green arrow)) 
 