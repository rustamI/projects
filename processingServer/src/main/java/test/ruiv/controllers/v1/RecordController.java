package test.ruiv.controllers.v1;

import test.ruiv.model.Model;
import test.ruiv.model.Record;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RecordController extends AbstractController {

    private static final String GET_ALL_RECORDS_QUERY = "SELECT * FROM record";
    @Override
    public Record create(Model value) {
        return null;
    }

    @Override
    public List create(List value) {
        return null;
    }

    @Override
    public Record update(Model value) {
        return null;
    }

    @Override
    public List update(List value) {
        return null;
    }

    @Override
    public boolean delete(BigInteger id) {
        return false;
    }

    @Override
    public Map<BigInteger, Boolean> delete(List id) {
        return null;
    }

    @Override
    public List<Record> findAll() {
        logger.trace("findAll() begin");
        List<Record> result = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement(GET_ALL_RECORDS_QUERY);
            ResultSet rs = preparedStatement.executeQuery();
            try {
                while (rs.next()) {
                    Record record = new Record.Builder()
                            .setRid(BigInteger.valueOf(rs.getLong("id")))
                            .setUid(BigInteger.valueOf(rs.getLong("uid")))
                            .setCurrency(rs.getString("currency"))
                            .setAmount(rs.getDouble("amount"))
                            .setCreated(rs.getTimestamp("created"))
                            .build();
                    result.add(record);
                }
            }finally {
                rs.close();
            }

        }catch (SQLException e) {
            logger.error("error ocurred", e);
        }
        logger.trace("findAll() end");
        return result;
    }

    @Override
    public Model find(String key) {
        return null;
    }

    @Override
    public List find(List keys) {
        return null;
    }
}
