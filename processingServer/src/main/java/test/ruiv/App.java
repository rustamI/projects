package test.ruiv;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.ruiv.datasources.H2DataSource;
import test.ruiv.dispatchers.DefaultDispatcher;
import test.ruiv.initializers.DbInitializer;

import static java.lang.System.out;

public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    private String[] args;
    private String hostname = "localhost";
    private int portNumber = 8081;
    private HttpServer httpServer;

    public App(String[] args) {
        logger.debug("input params: args - {}", args);
        this.args = args;
    }
    public static void main( String[] args ) throws Exception
    {
        App application = new App(args);
        application.init();
        new DbInitializer(new H2DataSource());
        application.start();
    }

    public void init(){
        logger.trace("initializers() begin");
        String hostname = "localhost";
        int port = 8081;
        if (args == null || args.length == 0) {
            out.println("Processing server has been started with default port localhost:8081");
            out.println("arguments [hostname] [port] ");
        } else {
            hostname = args[0];
            port = Integer.parseInt(args[1]);
            out.println("there was specified port - "+ portNumber);
        }
        try {
             httpServer = HttpServer.create(new InetSocketAddress(hostname, port), 0);
        }catch (IOException e) {
            logger.error("During server creation error occured", e);
        }
        new DefaultDispatcher(httpServer);
        httpServer.setExecutor(null); // creates a default executor
        logger.trace("initializers() end");
    }

    public void start() {
        this.httpServer.start();
    }

    public void close(){
        this.httpServer.stop(0);
    }
}
