package com.sbta.api.validators;

import com.sbta.api.validators.imp.AmountValidator;
import com.sbta.api.validators.imp.DateValidator;
import com.sbta.api.validators.imp.FormatValidator;
import org.springframework.stereotype.Component;

import java.util.Map;
@Component
public class ValidatorManager {
    private Validator start = null;

    /**
     * Default validation chain for essential check
     */
    public ValidatorManager(){
        Validator firstValidator = new FormatValidator();
        firstValidator.setNext(new DateValidator())
                .setNext(new AmountValidator());
        start = firstValidator;
    }

    /**
     * It allows to specify own chain
     * @param firstValidatorInChain
     */
    public ValidatorManager(Validator firstValidatorInChain){
        start = firstValidatorInChain;
    }

    public boolean check(Map<String, Object> context) throws Exception{
        // each thread may have either own checking strategy or default
        return start.validate(context);
    }
}
