package test.ruiv.model;

import org.h2.util.StringUtils;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Customer implements Serializable, Model {
    private BigInteger id;
    private String name;
    private Timestamp created;
    private Map<Object, Object> data;

    private Customer(){
    }

    public BigInteger getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Timestamp getCreated() {
        return created;
    }

    public Map<Object, Object> getData() {
        return data;
    }

    public void setParameter(Object key, Object value) {
        this.data.put(key,value);
    }

    public Object getParameter(Object key) {
        return this.data.get(key);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Customer)) return false;
        Customer customer = (Customer) o;
        return Objects.equals(getId(), customer.getId()) &&
                Objects.equals(getName(), customer.getName()) &&
                Objects.equals(getCreated(), customer.getCreated()) &&
                Objects.equals(getData(), customer.getData());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getCreated(), getData());
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", created=" + created +
                ", data=" + data +
                '}';
    }

    public static class Builder {

        private BigInteger uid;
        private String name;
        private Map<Object, Object> data = new HashMap<>();
        private Timestamp created;

        public Builder setUid(BigInteger uid) {
            this.uid = uid;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setData(Map<Object, Object> data) {
            if (data == null) {
                this.data = new HashMap<>();
            } else {
                this.data = data;
            }
            return this;
        }

        public Builder setCreated(Timestamp created) {
            this.created = created;
            return this;
        }

        public Customer build(){
            Customer customer = new Customer();
            customer.id = this.uid;
            customer.name = this.name;
            customer.created = this.created;
            customer.data = this.data;
            if (StringUtils.isNullOrEmpty(customer.name)) {
                throw new IllegalStateException("Customer's name cannot be empty or null");
            }
            return customer;
        }
    }
}
