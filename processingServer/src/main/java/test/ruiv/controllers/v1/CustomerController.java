package test.ruiv.controllers.v1;

import test.ruiv.model.Customer;
import test.ruiv.model.Model;
import test.ruiv.model.Transaction;

import java.math.BigInteger;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class CustomerController extends AbstractController {

    private static final String GET_ALL_CUSTOMERS_QUERY = "SELECT * FROM customer";
    private static final String GET_CUSTOMERS_BY_IDS_QUERY = "SELECT * FROM customer WHERE id in (?)";

    @Override
    public Customer create(Model value) {
        logger.trace("create() begin");
        logger.debug("input params: \nvalue - {}", value);
        Customer result = (Customer) value;
        return result;
    }

    @Override
    public List create(List value) {
        logger.trace("create() begin");
        logger.debug("input params: \nvalue - {}", value);
        List<Customer> result = (List<Customer>) value;
        return result;
    }

    @Override
    public Customer update(Model value) {
        return null;
    }

    @Override
    public List update(List value) {
        return null;
    }

    @Override
    public boolean delete(BigInteger id) {
        return false;
    }

    @Override
    public Map<BigInteger, Boolean> delete(List id) {
        logger.trace("delete() begin");
        return null;
    }

    @Override
    public List<Customer> findAll() {
        logger.trace("findAll() begin");
        List<Customer> result = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement(GET_ALL_CUSTOMERS_QUERY);

            ResultSet rs = preparedStatement.executeQuery();
            try {
                while (rs.next()) {
                    Customer customer = new Customer.Builder()
                            .setUid(BigInteger.valueOf(rs.getLong("id")))
                            .setName(rs.getString("name"))
                            //.setData()
                            .setCreated(rs.getTimestamp("created"))
                            .build();
                    result.add(customer);
                }
            } finally {
                rs.close();
            }

        } catch (SQLException e) {
            logger.error("error ocurred", e);
        }
        logger.trace("findAll() end");
        return result;
    }

    @Override
    public Customer find(String key) {
        return find(Collections.singletonList(key)).get(0);
    }

    @Override
    public List<Customer> find(List keys) {
        logger.trace("find() begin");
        logger.debug("input params: \n keys - {}", keys);
        List<Customer> result = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement(GET_CUSTOMERS_BY_IDS_QUERY);
            preparedStatement.setArray(1, getConnection().createArrayOf("int", keys.toArray()));
            ResultSet rs = preparedStatement.executeQuery();
            try {
                while (rs.next()) {
                    Customer customer = new Customer.Builder()
                            .setUid(BigInteger.valueOf(rs.getLong("id")))
                            .setName(rs.getString("name"))
                            //.setData()
                            .setCreated(rs.getTimestamp("created"))
                            .build();
                    result.add(customer);
                }
            } finally {
                rs.close();
            }
        } catch (SQLException e) {
            logger.error("find() error ocurred", e);
        }
        logger.trace("find() end");
        return result;
    }

    public BigInteger transfer(Transaction transaction) {
        logger.trace("transfer() begin");
        logger.debug("input params: \n transaction - {}", transaction);
        BigInteger result = null;
        BigInteger source_rid = transaction.getSrid();
        BigInteger destination_rid = transaction.getDrid();
        Double rate = transaction.getRate();
        Double value = transaction.getValue();
        // in case of negative value source currency is less than destination
        Double finalValue = value;
        if (rate > 0) {
            finalValue = value*rate;
        } else if (rate<0){
            finalValue =  value/rate;
        }

        Connection connection = getConnection();
        try {
            connection.setAutoCommit(false);
            connection.prepareStatement("UPDATE record SET amount = amount + " + finalValue +" where id = " + destination_rid).execute();
            connection.prepareStatement("UPDATE record SET amount = amount - " + value +" where id = " + source_rid).execute();
            PreparedStatement preparedStatement = connection.prepareStatement("insert into transaction (id, srid, drid, rate, value, created)" +
                    "\n values (null, "+ source_rid + ", "+destination_rid+", "+rate+", "+value+", CURRENT_DATE())", Statement.RETURN_GENERATED_KEYS);
            //preparedStatement.setString(1, "id");
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                result = BigInteger.valueOf(rs.getLong(1));
            }
            connection.commit();
        } catch (Exception e) {
            logger.error("something went wrong during transfer", e);
            try {
                connection.rollback();
            }catch (SQLException se) {
                logger.error("rollback failed ", se);
            }
            return BigInteger.ZERO;
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (Exception e) {
                logger.error("setAutoCommit - true failed", e);
            }
        }
        return result;
    }
}
