package com.sbta.api.validators.imp;

import com.sbta.api.validators.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public abstract class AbstractValidator implements Validator {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    protected Validator next;

    /**
     * It launches the validation process by given context
     * @param context - read/write context
     * @return true in case of all is good
     * @throws Exception any type of unexpected
     */
    @Override
    public boolean validate(Map context) throws Exception {
        logger.trace("validate() begin");
        boolean result = doValidation(context);
        if (next != null){
            try{
                result = next.validate(context);
            }catch (Exception e){
                logger.error("validation error occurs", e);
                result = false;
                context.put("status", Status.ERROR_OCCURRED);
            }
        }
        logger.debug("returns: result - {}", result);
        logger.trace("validate() end");
        return result;
    }

    protected abstract boolean doValidation(Map context);

    @Override
    public Validator setNext(Validator validator) {
        logger.trace("setNext() begin");
        this.next = validator;
        logger.trace("setNext() end");
        return this.next;
    }


}
