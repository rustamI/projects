package com.sbta.api;

import com.sbta.api.holders.DataHolder;
import com.sbta.api.validators.ValidatorManager;
import com.sbta.models.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class Collector {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ValidatorManager validatorManager;

    @Autowired
    DataHolder dataHolder;

    public ResponseEntity add(String data) {
        logger.trace("add() begin");
        ResponseEntity<String> responseEntity = null;
        Map<String, Object> context = new HashMap<>();
        context.put("data", data);
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            if (validatorManager.check(context)) {
                logger.debug("all is correct");
                Transaction transaction = (Transaction) context.get("transaction");
                dataHolder.push(transaction);
                responseEntity = new ResponseEntity(httpStatus);
            } else {
                logger.debug("validation failed\ncontext - {}", context);
                responseEntity = new ResponseEntity((HttpStatus) context.get("httpStatus"));
            }
        }catch (Exception e){
            responseEntity = new ResponseEntity(HttpStatus.BAD_REQUEST);
            logger.error("error during validation process",e);
        }

        logger.trace("add() end");
        return responseEntity;
    }

    public void clear(){
        dataHolder.clear();
    }

}
