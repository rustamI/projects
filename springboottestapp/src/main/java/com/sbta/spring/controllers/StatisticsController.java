package com.sbta.spring.controllers;

import com.sbta.api.holders.DataHolder;
import com.sbta.models.Statistic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class StatisticsController {
    private static final Logger logger = LoggerFactory.getLogger(StatisticsController.class);
    @Autowired
    private DataHolder dataHolder;

    @RequestMapping(value = "/statistics", method = RequestMethod.GET, headers = "Accept=application/json")
    public Statistic get() {
        logger.trace("get() begin");
        Statistic result = dataHolder.getStatistic();
        logger.trace("get() begin");
        return result;
    }
}
