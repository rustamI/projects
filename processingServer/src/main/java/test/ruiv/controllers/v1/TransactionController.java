package test.ruiv.controllers.v1;

import test.ruiv.model.Model;
import test.ruiv.model.Transaction;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TransactionController extends AbstractController {

    private static final String GET_ALL_TRANSACTIONS_QUERY = "SELECT * FROM transaction";
    @Override
    public Transaction create(Model value) {
        return null;
    }

    @Override
    public List create(List value) {
        return null;
    }

    @Override
    public Transaction update(Model value) {
        return null;
    }

    @Override
    public List update(List value) {
        return null;
    }

    @Override
    public boolean delete(BigInteger id) {
        return false;
    }

    @Override
    public Map<BigInteger, Boolean> delete(List id) {
        return null;
    }

    @Override
    public List<Transaction> findAll() {
        logger.trace("findAll() begin");
        List<Transaction> result = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement(GET_ALL_TRANSACTIONS_QUERY);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Transaction transaction = new Transaction.Builder()
                        .setTid(BigInteger.valueOf(rs.getLong("id")))
                        .setSrid(BigInteger.valueOf(rs.getLong("srid")))
                        .setDrid(BigInteger.valueOf(rs.getLong("drid")))
                        .setRate(rs.getDouble("rate"))
                        .setValue(rs.getDouble("value"))
                        .setCreated(rs.getTimestamp("created"))
                        .build();
                result.add(transaction);
            }

        }catch (SQLException e) {
            logger.error("error ocurred", e);
        }
        logger.trace("findAll() end");
        return result;
    }

    @Override
    public Model find(String key) {
        return null;
    }

    @Override
    public List find(List keys) {
        return null;
    }
}
