package com.sbta.api.utils;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Id generator for transactions
 */
public class ID {
    private static AtomicLong nextId = new AtomicLong(0);
    public static long getNextId() {
        return nextId.getAndIncrement();
    }
}
