package test.ruiv.model;

import org.h2.util.StringUtils;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Objects;

public class Record implements Serializable, Model {
    private BigInteger id;
    private BigInteger uid;
    private String currency; // ISO 4217
    private Double amount;
    private Timestamp created;

    private Record(){}

    public BigInteger getId() {
        return this.id;
    }

    public BigInteger getUid() {
        return this.uid;
    }

    public String getCurrency() {
        return this.currency;
    }

    public Double getAmount() {
        return this.amount;
    }

    public Timestamp getCreated() { return this.created; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Record)) return false;
        Record record = (Record) o;
        return Objects.equals(getId(), record.getId()) &&
                Objects.equals(getUid(), record.getUid()) &&
                Objects.equals(getCurrency(), record.getCurrency()) &&
                Objects.equals(getAmount(), record.getAmount());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUid(), getCurrency(), getAmount());
    }

    @Override
    public String toString() {
        return "Record{" +
                "id=" + id +
                ", uid=" + uid +
                ", currency='" + currency + '\'' +
                ", amount=" + amount +
                ", created=" + created +
                '}';
    }

    public static class Builder {
        private BigInteger rid;
        private BigInteger uid;
        private String currency;
        private Double amount;
        private Timestamp created;

        public Builder setRid(BigInteger rid) {
            this.rid = rid;
            return this;
        }

        public Builder setUid(BigInteger uid) {
            this.uid = uid;
            return this;
        }

        public Builder setCurrency(String currency) {
            this.currency = currency;
            return this;
        }

        public Builder setAmount(Double amount) {
            this.amount = amount;
            return this;
        }


        public Builder setCreated(Timestamp created) {
            this.created = created;
            return this;
        }

        public Record build() {
            Record record = new Record();
            record.id = this.rid;
            record.uid = this.uid;
            record.currency = this.currency;
            record.amount = this.amount;
            record.created = this.created;
            if (record.uid == null) {
                throw new IllegalStateException("Customer's UID cannot be null");
            }
            if (StringUtils.isNullOrEmpty(record.currency)) {
                throw new IllegalStateException("Record must have currency definition");
            }
            return record;
        }
    }
}
