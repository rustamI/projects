package test.ruiv.initializers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.ruiv.datasources.H2DataSource;

import java.sql.Connection;

/**
 * http://www.h2database.com/html/grammar.html
 * http://www.h2database.com/html/datatypes.html
 * it applies dump to the DB
 */
public class DbInitializer {
    protected static final Logger logger = LoggerFactory.getLogger(DbInitializer.class);
    private Connection connection;
    public DbInitializer(H2DataSource h2DataSource) throws Exception {
        logger.trace("DbInitializer started");
        connection = h2DataSource.getConnection();
        connection.setAutoCommit(false);
        createTables();
        createUsers();
        createRecords();
        createTransactions();
        connection.commit();
        connection.setAutoCommit(true);
        logger.trace("DbInitializer end");
    }

    private void setDbConfiguration() throws Exception{
        logger.trace("db configs is done");
        connection.prepareStatement("SET COLLATION charset_cp1251").execute();
    }
    private void createTables() throws Exception {
        connection.prepareStatement("create table customer (id int primary key auto_increment(324), name varchar(100), created timestamp)").execute();
        connection.prepareStatement("create table record (id int primary key auto_increment(456), uid int, currency varchar(3) COMMENT 'ISO 4217', amount double, created timestamp)").execute();
        connection.prepareStatement("create table transaction (id int primary key auto_increment(232), srid int NOT NULL, drid int, rate double, value double, created timestamp)").execute();
        logger.trace("tables created");
    }

    private void createUsers() throws Exception{
        connection.prepareStatement("insert into customer (id, name, created) values (null, 'test name', '2005-12-31 23:59:59Z');").execute();
        connection.prepareStatement("insert into customer (id, name, created) values (null, 'test name', '2015-2-23 23:59:59Z');").execute();
        connection.prepareStatement("insert into customer (id, name, created) values (null, 'test name', '2016-11-21 23:59:59Z');").execute();
        connection.prepareStatement("insert into customer (id, name, created) values (null, 'test name', '2014-3-11 23:59:59Z');").execute();
        logger.trace("users created");
    }

    private void createRecords() throws Exception{
        connection.prepareStatement("insert into record (id, uid, currency, amount, created) " +
                        "\n values (null, 325, 'USD', '20', '2016-11-21 23:59:59Z')").execute();
        connection.prepareStatement("insert into record (id, uid, currency, amount, created) " +
                "\n values (null, 326, 'USD', '50', '2016-11-21 23:59:59Z')").execute();
        logger.trace("Records created");
    }

    private void createTransactions() throws Exception {
        connection.prepareStatement("insert into transaction (id, srid, drid, rate, value, created)" +
                "\n values (null, 457, 458, 0.0, 5, '2018-3-11 23:59:59Z')").execute();
        logger.trace("transactions created");
    }
}
