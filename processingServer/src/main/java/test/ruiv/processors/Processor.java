package test.ruiv.processors;

import com.sun.net.httpserver.HttpExchange;

import java.net.URI;
import java.util.Map;

public interface Processor {
    String getValue(HttpExchange httpExchange, Map<String, Object> data) throws Exception;
    Map<String, String> getUrlParams(URI uri);
}
