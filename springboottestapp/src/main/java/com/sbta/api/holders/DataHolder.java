package com.sbta.api.holders;

import com.sbta.api.utils.Converter;
import com.sbta.models.Statistic;
import com.sbta.models.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Singleton thread-safe class to store income transactions.
 * It removes old transactions to prevent OOM
 */
@Component
public class DataHolder {
    private static final Logger logger = LoggerFactory.getLogger(DataHolder.class);
    private Collection<Transaction> transactions = new CopyOnWriteArrayList<>();
    private static DataHolder instance = null;
    private Boolean cleaningIsUp = false;

    private DataHolder() {
    }

    public static synchronized DataHolder getInstance() {
        logger.trace("getInstance() begin");
        if (instance == null) {
            instance = new DataHolder();
        }
        logger.debug("returns: instance - {}", instance);
        logger.trace("getInstance() end");
        return instance;
    }

    /**
     * Inserts the specified element into this transactions if it is possible to do
     * so immediately without violating capacity restrictions
     *
     * @param transaction
     */
    public void push(Transaction transaction) {
        logger.trace("push() begin");
        logger.debug("input params: transaction - {}", transaction);
        transactions.add(transaction);
        if (!cleaningIsUp) {
            cleaningIsUp = true;
            // it could be changed
            new Thread(new CleaningThreadRunnable(cleaningIsUp, Instant.now())).start();
        }
        logger.trace("push() end");
    }

    /**
     * Clears all transactions from transactions
     */
    public void clear() {
        logger.trace("clear() begin");
        transactions.clear();
        logger.trace("clear() end");
    }

    /**
     * It returns statistic based on elements in the transactions
     *
     * @return {@link Statistic}
     */
    public Statistic getStatistic() {
        logger.trace("getStatistic() begin");
        Iterator<Transaction> iterator = transactions.iterator();
        Statistic result = null;
        // time in sec
        long offset = 60L;
        Instant instantNowMinusOffset = Instant.now().minusSeconds(offset);
        Instant instantNow = Instant.now();
        BigDecimal sum = BigDecimal.ZERO,
                avg = BigDecimal.ZERO,
                max = BigDecimal.ZERO,
                min = null;
        int count = 0;

        while (iterator.hasNext()) {
            Transaction transaction = iterator.next();
            logger.debug("current transaction -{}", transaction);
            Instant timeStamp = Instant.parse(transaction.getTimestamp());
            BigDecimal amount = new BigDecimal(transaction.getAmount());

            if (instantNowMinusOffset.isBefore(timeStamp) && instantNow.isAfter(timeStamp)) {
                count++;
                //min = amount;
                sum = sum.add(amount);
                if (max.compareTo(amount) <= 0) {
                    max = amount;
                }
                if (min == null || min.compareTo(amount) >= 0) {
                    min = amount;
                }
                if (count != 0) {
                    avg = sum.divide(BigDecimal.valueOf(count), 2, RoundingMode.HALF_UP);
                } else {
                    avg = amount;
                }
            } else {
                transactions.remove(transaction);
            }
        }
        if (result == null) {
            // the case when no one is withing 60 sec timeframe
            if (min == null) {
                min = BigDecimal.ZERO;
            }
            result = new Statistic(Converter.round(sum), Converter.round(avg), Converter.round(max), Converter.round(min), count);
        }
        logger.trace("getStatistic() end");
        return result;
    }

    private class CleaningThreadRunnable implements Runnable {
        private Boolean cleaningIsUp;
        private Instant instant;

        public CleaningThreadRunnable(Boolean cleaningIsUp, Instant instant) {
            this.cleaningIsUp = cleaningIsUp;
            this.instant = instant;
        }

        @Override
        public void run() {
            logger.trace("CleaningThreadRunnable started");
            Iterator<Transaction> iterator = transactions.iterator();
            // time in sec
            long offset = 60L;
            Instant instantNowMinusOffset = instant.minusSeconds(offset);
            Instant instantNow = instant;
            while (iterator.hasNext()) {
                Transaction transaction = iterator.next();
                logger.debug("current transaction -{}", transaction);
                Instant timeStamp = Instant.parse(transaction.getTimestamp());
                if (instantNowMinusOffset.isBefore(timeStamp) && instantNow.isAfter(timeStamp)) {
                } else {
                    transactions.remove(transaction);
                }
            }
            this.cleaningIsUp = false;
            logger.trace("CleaningThreadRunnable end");
        }
    }

}
