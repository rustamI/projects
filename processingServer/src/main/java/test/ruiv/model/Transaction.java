package test.ruiv.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Objects;

public class Transaction implements Serializable, Model {
    private BigInteger id;
    private BigInteger srid;
    private BigInteger drid;
    private Double rate;
    private Double value;
    private Timestamp created;

    private Transaction(){}

    public BigInteger getId() {
        return id;
    }

    public BigInteger getSrid() {
        return srid;
    }

    public BigInteger getDrid() {
        return drid;
    }

    public Double getRate() {
        return rate;
    }

    public Double getValue() {
        return value;
    }

    public Timestamp getCreated() {
        return created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transaction)) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getSrid(), that.getSrid()) &&
                Objects.equals(getDrid(), that.getDrid()) &&
                Objects.equals(getRate(), that.getRate()) &&
                Objects.equals(getCreated(), that.getCreated());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getSrid(), getDrid(), getRate(), getCreated());
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", srid=" + srid +
                ", drid=" + drid +
                ", rate=" + rate +
                ", value=" + value +
                ", created=" + created +
                '}';
    }

    public static class Builder {
        private BigInteger tid;
        private BigInteger srid;
        private BigInteger drid;
        private Double rate;
        private Double value;
        private Timestamp created;

        public Builder setTid(BigInteger tid) {
            this.tid = tid;
            return this;
        }

        public Builder setSrid(BigInteger srid) {
            this.srid = srid;
            return this;
        }

        public Builder setDrid(BigInteger drid) {
            this.drid = drid;
            return this;
        }

        public Builder setRate(Double rate) {
            this.rate = rate;
            return this;
        }

        public Builder setValue(Double value) {
            this.value = value;
            return this;
        }

        public Builder setCreated(Timestamp created) {
            this.created = created;
            return this;
        }

        public Transaction build() {
            Transaction transaction = new Transaction();
            transaction.id = this.tid;
            transaction.srid = this.srid;
            transaction.drid = this.drid;
            transaction.rate = this.rate;
            transaction.value = this.value;
            transaction.created = this.created;

            return  transaction;
        }
    }
}
