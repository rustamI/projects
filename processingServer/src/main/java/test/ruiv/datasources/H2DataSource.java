package test.ruiv.datasources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class H2DataSource {
    protected static final Logger logger = LoggerFactory.getLogger(H2DataSource.class);
    private Connection connection;
    private String connectionPath = "jdbc:h2:mem:db1";
    static {
        org.h2.Driver.load();
    }

    public H2DataSource() {
        logger.trace("default constructor invoked");
    }

    public H2DataSource(String connectionPath) {
        logger.trace("constuctor invoked");
        logger.debug("connectionPath has been specified, new value - {}", connectionPath);
        this.connectionPath = connectionPath;
    }

    public Connection getConnection() throws SQLException {
        logger.trace("getConnection()");
        this.connection = DriverManager.getConnection(connectionPath);
        return this.connection;
    }

    public Connection getConnection(String username, String password) throws SQLException {
        logger.trace("getConnection(username, password)");
        return DriverManager.getConnection(username, password, connectionPath);
    }
}
