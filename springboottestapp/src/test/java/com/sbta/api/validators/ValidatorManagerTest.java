package com.sbta.api.validators;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ValidatorManagerTest {

    @Autowired
    ValidatorManager validatorManager;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void checkFormatValidator() {
        System.out.println("checkFormatValidator() begin");
        Map<String, Object> context = new HashMap<>();
        try{
            // emtpy context
            assertFalse(validatorManager.check(context));
            context.put("data", "{\"amount__\": \"12.3343\",\"timestamp\": \"2018-10-01T10:02:51.312Z\"}");
            assertFalse(validatorManager.check(context) );
            System.out.println("httpStatus - " + context.get("httpStatus"));
        } catch (Exception e){
            fail();
        }
        System.out.println("checkFormatValidator() end");
    }

    @Test
    public void checkValidatorSuccess(){
        System.out.println("checkValidatorSuccess() begin");
        Map<String, Object> context = new HashMap<>();
        context.put("data","{\"amount\": \"12.3343\",\"timestamp\": \""+ Instant.now().toString() +"\"}");
        try{
            assertTrue(validatorManager.check(context));
        }catch (Exception e){
            fail();
        }
        System.out.println("checkValidatorSuccess() end");
    }
}