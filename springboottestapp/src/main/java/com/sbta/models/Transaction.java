package com.sbta.models;

import com.sbta.api.utils.ID;

import java.util.Objects;

public class Transaction {

    private String amount;
    private String timestamp;
    private Long tid = ID.getNextId();

    public Transaction(){}
    public Transaction(String amount, String timestamp){
        this.amount = amount;
        this.timestamp = timestamp;
    }

    public String getAmount() {
        return this.amount;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public Transaction setAmount(String amount) {
        this.amount = amount;
        return this;
    }

    public Transaction setTimestamp(String timestamp) {
        this.timestamp = timestamp;
        return this;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transaction)) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(getAmount(), that.getAmount()) &&
                Objects.equals(getTimestamp(), that.getTimestamp()) &&
                Objects.equals(tid, that.tid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAmount(), getTimestamp(), tid);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "amount='" + amount + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", tid=" + tid +
                '}';
    }
}
