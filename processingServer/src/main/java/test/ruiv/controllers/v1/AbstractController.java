package test.ruiv.controllers.v1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.ruiv.datasources.H2DataSource;
import test.ruiv.model.Model;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public abstract class AbstractController<T extends Model> {
    protected static final Logger logger = LoggerFactory.getLogger(AbstractController.class);
    private Connection connection;

    public AbstractController(){
        try {
            connection = new H2DataSource().getConnection();
        } catch (SQLException e) {
            logger.error("connection initializers failed", e);
        }
    }

    public AbstractController(Connection connection) {
        this.connection = connection;
    }

    public abstract T create(T value);
    public abstract List<T> create(List<T> value);
    public abstract T update(T value);
    public abstract List<T> update(List<T> value);
    public abstract boolean delete(BigInteger id);
    public abstract Map<BigInteger,Boolean> delete(List<BigInteger> id);
    public abstract List<T> findAll();
    public abstract T find(String key);
    public abstract List<T> find(List<String> keys);
    public Connection getConnection(){
        Connection result = this.connection;
        if (connection == null) {
            try {
                this.connection = new H2DataSource().getConnection();
            } catch (SQLException e) {
                logger.error("connection initializers failed", e);
            }
        }
        return result;
    }
}
