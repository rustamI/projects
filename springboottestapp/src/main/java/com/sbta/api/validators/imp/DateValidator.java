package com.sbta.api.validators.imp;

import com.sbta.models.Transaction;
import org.springframework.http.HttpStatus;

import java.time.DateTimeException;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.Map;

public class DateValidator extends AbstractValidator {

    @Override
    protected boolean doValidation(Map context) {
        logger.trace("doValidation() begin");
        Instant currentTime = Instant.now();
        Transaction incomeTransaction = (Transaction) context.get("transaction");
        try {
            Instant timestamp = Instant.parse(incomeTransaction.getTimestamp());
            if (currentTime.isBefore(timestamp)) {
                throw new DateTimeParseException("This timestamp in the future", timestamp.toString(), 1);
            } else if (currentTime.minusSeconds(60L).isAfter(timestamp)) {
                throw new DateTimeException("This timestamp in the past");
            }
        } catch (DateTimeParseException e) {
            //422 – if any of the fields are not parsable or the transaction date is in the future
            context.put("httpStatus", HttpStatus.UNPROCESSABLE_ENTITY);
            context.put("errorInfo", e);
            next = null;
            logger.trace("doValidation() end");
            return false;

        } catch (DateTimeException e1) {
            //204 – if the transaction is older than 60 seconds
            context.put("httpStatus", HttpStatus.NO_CONTENT);
            context.put("errorInfo", e1);
            next = null;
            logger.trace("doValidation() end");
            return false;
        }
        logger.trace("doValidation() end");
        return true;
    }
}
